import 'dart:io';
import 'package:cake_app/Constants/route.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image_picker/image_picker.dart';

import 'Constants/route.dart';

class AddCakes extends StatefulWidget {
  static final routeName = '/addCakes';
  @override
  _AddCakesState createState() => _AddCakesState();
}

class _AddCakesState extends State<AddCakes> {
  final _addUserScreenFormKey = GlobalKey<FormState>();
  String id;
  TextEditingController _nameCtrl = TextEditingController();
  TextEditingController _priceCtrl = TextEditingController();
  TextEditingController _timeCtrl = TextEditingController();
  PickedFile imagePicked;
  final firestoreInstance = FirebaseFirestore.instance;
  User user = FirebaseAuth.instance.currentUser;
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context,
        designSize: Size(2280, 1080), allowFontScaling: true);
    return Scaffold(
      appBar: AppBar(
        title: Text('Add New Cakes'),
        leading: IconButton(icon: Icon(Icons.arrow_back),onPressed: (){
          Navigator.pushNamed(context, RouteNames.adminHome);
        },
        ),
      ),
      body: SafeArea(
        
        child: SingleChildScrollView(
          child: Center(
            child: Form(
              key: _addUserScreenFormKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  buildName(),
                  buildPrice(),
                  buildPreparationTime(),
                  buildImage(),
                  buildSubmitButton(context),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
  buildImage() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        if (imagePicked != null)
          SizedBox(
            width: 200.h,
            height: 200.h,
            child: Image.file(File(imagePicked.path)),
          ),
        RaisedButton(
          child: Text("Pick Image/Logo"),
          onPressed: () async {
            var image =
            await ImagePicker().getImage(source: ImageSource.gallery);
            setState(() {
              imagePicked = image;
            });
          },
        ),
      ],
    );
  }

  Padding buildSubmitButton(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Align(
        alignment: Alignment.bottomRight,
        child: RaisedButton(
          color: Theme.of(context).primaryColor,
          child: Text(
            "Add",
            style: TextStyle(color: Colors.white),
          ),
          onPressed: () {
            FocusScope.of(context).unfocus();
            if (_addUserScreenFormKey.currentState.validate()) {
              print('imagePicked $imagePicked');
              if (imagePicked == null) {
                showDialog(
                  context: context,
                  child: AlertDialog(
                    title: Text('Select Image / Logo'),
                    actions: [
                      FlatButton(
                        onPressed: () => Navigator.pop(context),
                        child: Text('Ok'),
                      )
                    ],
                  ),
                );
              } else {
                createCakes();
              }
            }
          },
        ),
      ),
    );
  }

  createCakes() {
    FirebaseStorage.instance
        .ref()
        .child('/bakery_logos/${_nameCtrl.text}')
        .putFile(File(imagePicked.path))
        .onComplete
        .then((image) {
      image.ref.getDownloadURL().then((link) {
        FirebaseFirestore.instance
            .collection('users').doc(user.uid).collection("cakes")
            .add({
          'Cake_Name': _nameCtrl.text,
          'Img': link,
          'Price': _priceCtrl.text,
          'preparation_time' : _timeCtrl.text,
        }
        );

      }
      );
    });
  }
  Padding buildName() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        controller: _nameCtrl,
        validator: (value) {
          if (value.isEmpty) {
            return 'Enter the field';
          }
          return null;
        },
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          labelText: 'Cake Name',
        ),
      ),
    );
  }
  Padding buildPrice() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        controller: _priceCtrl,
        validator: (value) {
          if (value.isEmpty) {
            return 'Enter the field';
          }
          return null;
        },
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          labelText: 'Price',
        ),
      ),
    );
  }

  Padding buildPreparationTime() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        controller: _timeCtrl,
        validator: (value) {
          if (value.isEmpty) {
            return 'Enter the field';
          }
          return null;
        },
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          labelText: 'Preparation-Time',
        ),
      ),
    );
  }


}

