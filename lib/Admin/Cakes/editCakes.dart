

import 'dart:io';
import 'package:cake_app/Constants/route.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image_picker/image_picker.dart';
class EditCakes extends StatefulWidget {
  static final routeName = '/editCakes';
  final String Cake_Name;

  const EditCakes({Key key, this.Cake_Name}) : super(key: key);
  @override
  _EditCakesState createState() => _EditCakesState();
}

class _EditCakesState extends State<EditCakes> {
 
  final _editScreenFormKey = GlobalKey<FormState>();
  TextEditingController _nameCtrl = TextEditingController();
  TextEditingController _priceCtrl = TextEditingController();
  TextEditingController _timeCtrl = TextEditingController();
  PickedFile imagePicked;
  final firestoreInstance = FirebaseFirestore.instance;
  User user = FirebaseAuth.instance.currentUser;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Cake Details"),
        ),
        body: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: StreamBuilder(
              stream: FirebaseFirestore.instance.collection("users").doc(user.uid).collection("cakes").snapshots(),

              builder: (context, snapshot) {
                if(snapshot.connectionState == ConnectionState.waiting){
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                  
                }
                else {
              
                  return Form(
                    key: _editScreenFormKey,
                    child: Column(
                      children: [
                        buildName(),
                        buildPrice(),
                        buildPreparationTime(),
                        buildImage(),
                Padding(
                padding: const EdgeInsets.all(10.0),
                child: Align(
                alignment: Alignment.bottomRight,
                child: RaisedButton(
                color: Theme.of(context).primaryColor,
                child: Text(
                "Update",
                style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                FocusScope.of(context).unfocus();
                if (_editScreenFormKey.currentState.validate()) {
                print('imagePicked $imagePicked');
                if (imagePicked == null) {
                showDialog(
                context: context,
                child: AlertDialog(
                title: Text('Select Image / Logo'),
                actions: [
                FlatButton(
                onPressed: () => Navigator.pop(context),
                child: Text('Ok'),
                )
                ],
                ),
                );
                } else {
                FirebaseStorage.instance
                      .ref()
                      .child('/bakery_logos/${_nameCtrl.text}')
                      .putFile(File(imagePicked.path))
                      .onComplete
                      .then((image) {
                image.ref.getDownloadURL().then((link) {
                FirebaseFirestore.instance
                      .collection('users').doc(user.uid).collection("cakes").doc(
                  // TODO: get documentID
                )
                      .update({
                'Cake_Name': widget.Cake_Name,
                'Img': link,
                'Price': _priceCtrl.text,
                'preparation_time' : _timeCtrl.text,
                }
                );

                }
                );
                });
                }
                }
                },
                ),
                ),
                )
                      ],
                    ),
                  );
                }
              }
          ),
        ),
      ),
    );
  }

  buildImage() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        if (imagePicked != null)
          SizedBox(
            width: 200.h,
            height: 200.h,
            child: Image.file(File(imagePicked.path)),
          ),
        RaisedButton(
          child: Text("Pick Image/Logo"),
          onPressed: () async {
            var image =
            await ImagePicker().getImage(source: ImageSource.gallery);
            setState(() {
              imagePicked = image;
            });
          },
        ),
      ],
    );
  }

  Padding buildName() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        controller: _nameCtrl,
        validator: (value) {
          if (value.isEmpty) {
            return 'Enter the field';
          }
          return null;
        },
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          labelText: 'Cake Name',

        ),
      ),
    );
  }
  Padding buildPrice() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        controller: _priceCtrl,
        validator: (value) {
          if (value.isEmpty) {
            return 'Enter the field';
          }
          return null;
        },
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          labelText: 'Price',
        ),
      ),
    );
  }

  Padding buildPreparationTime() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        controller: _timeCtrl,
        validator: (value) {
          if (value.isEmpty) {
            return 'Enter the field';
          }
          return null;
        },
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          labelText: 'Preparation-Time',
        ),
      ),
    );
  }

}