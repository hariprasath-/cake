import 'dart:async';

import 'package:cake_app/Constants/route.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SplashScreen extends StatefulWidget {
  static final routeName = '/splash';

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  initState() {
    super.initState();
    var user = FirebaseAuth.instance.currentUser;
    if (user == null) {
      Timer(
          Duration(seconds: 2),
          () =>
              Navigator.pushReplacementNamed(context, RouteNames.loginScreen));
    } else {
      FirebaseFirestore.instance
          .collection('users')
          .doc(user.uid)
          .get()
          .then((userData) {
        if (userData['type'] == 'super_admin') {
          Timer(
              Duration(seconds: 2),
              () => Navigator.pushReplacementNamed(
                  context, RouteNames.superAdminHome));
        }
        if (userData['type'] == 'admin') {
          Timer(
              Duration(seconds: 2),
                  () => Navigator.pushReplacementNamed(
                  context, RouteNames.adminHome));
        }
        if (userData['type'] == 'staff') {
          // Nav to staff....
        }
        if (userData['type'] == 'user') {
          // Nav to user....
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context,
        designSize: Size(2280, 1080), allowFontScaling: true);
    return SafeArea(
      child: Scaffold(
        body: Center(
          child: SizedBox(
            height: 0.4.wp,
            width: 0.4.wp,
            child: FittedBox(
              child: Container(
                child: FlutterLogo(),
                color: Colors.pink,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
