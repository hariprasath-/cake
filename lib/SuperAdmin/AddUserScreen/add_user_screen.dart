import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image_picker/image_picker.dart';

class AddUserScreen extends StatefulWidget {
  static final routeName = '/addUser';

  const AddUserScreen({Key key}) : super(key: key);

  @override
  _AddUserScreenState createState() => _AddUserScreenState();
}

class _AddUserScreenState extends State<AddUserScreen> {

  final _addUserScreenFormKey = GlobalKey<FormState>();
  TextEditingController _nameCtrl = TextEditingController();
  TextEditingController _addressCtrl = TextEditingController();
  Color _colorData = Color(0xFFEC407A);
  Color currentColor = Color(0xFFEC407A);
  TextEditingController _phoneCtrl = TextEditingController();
  String _typeData = 'super_admin';
  PickedFile imagePicked;
  TextEditingController _otpCtrl = TextEditingController();
  bool userCreated = false;

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context,
        designSize: Size(2280, 1080), allowFontScaling: true);
    return Scaffold(
      appBar: AppBar(
        title: Text('Add User'),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Center(
            child: Form(
              key: _addUserScreenFormKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  buildName(),
                  buildPhone(),
                  buildType(),
                  buildColor(context),
                  buildAddress(),
                  buildImage(),
                  buildSubmitButton(context),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  buildImage() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        if (imagePicked != null)
          SizedBox(
            width: 200.h,
            height: 200.h,
            child: Image.file(File(imagePicked.path)),
          ),
        RaisedButton(
          child: Text("Pick Image/Logo"),
          onPressed: () async {
            var image =
                await ImagePicker().getImage(source: ImageSource.gallery);
            setState(() {
              imagePicked = image;
            });
          },
        ),
      ],
    );
  }

  Padding buildSubmitButton(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Align(
        alignment: Alignment.bottomRight,
        child: RaisedButton(
          color: Theme.of(context).primaryColor,
          child: Text(
            "Add",
            style: TextStyle(color: Colors.white),
          ),
          onPressed: () {
            FocusScope.of(context).unfocus();
            if (_addUserScreenFormKey.currentState.validate()) {
              print('imagePicked $imagePicked');
              if (imagePicked == null) {
                showDialog(
                  context: context,
                  child: AlertDialog(
                    title: Text('Select Image / Logo'),
                    actions: [
                      FlatButton(
                        onPressed: () => Navigator.pop(context),
                        child: Text('Ok'),
                      )
                    ],
                  ),
                );
              } else {
                createUser();
              }
            }
          },
        ),
      ),
    );
  }

  createUser() {
    bool otp = false;
    Firebase.initializeApp(
      options: Firebase.app().options,
    ).then((authApp) {
      FirebaseAuth.instanceFor(app: authApp).verifyPhoneNumber(
        phoneNumber: _phoneCtrl.text.trim(),
        timeout: Duration(seconds: 60),
        verificationCompleted: (phoneAuthCredential) {
          print("Auth Created");
        },
        verificationFailed: (error) {
          print(error);
        },
        codeSent: (verificationId, forceResendingToken) {
          print("Manual");

          showDialog(
            context: context,
            child: AlertDialog(
              title: Text("Enter OTP"),
              content: TextFormField(
                controller: _otpCtrl,
                keyboardType: TextInputType.number,
              ),
              actions: [
                FlatButton(
                  onPressed: () {
                    Navigator.pop(context, true);
                    AuthCredential credential = PhoneAuthProvider.credential(
                        verificationId: verificationId, smsCode: _otpCtrl.text);
                    FirebaseAuth.instanceFor(app: authApp)
                        .signInWithCredential(credential)
                        .then((user) {
                      print("Auth Created");
                      FirebaseStorage.instance
                          .ref()
                          .child('/images_logos/${_phoneCtrl.text}')
                          .putFile(File(imagePicked.path))
                          .onComplete
                          .then((image) {
                        image.ref.getDownloadURL().then((link) {
                          FirebaseFirestore.instanceFor(app: authApp)
                              .collection('users')
                              .doc(user.user.uid)
                              .set({
                            'name': _nameCtrl.text,
                            'type': _typeData,
                            'number': _phoneCtrl.text,
                            'address': _addressCtrl.text,
                            'image_logo': link,
                            'color':currentColor.toString(),
                          }).then((value) {
                            print("DB Created");
                            FirebaseAuth.instanceFor(app: authApp).signOut();
                          });
                        });
                      });
                    });
                  },
                  child: Text("Verify"),
                ),
              ],
            ),
          ).then((value) => otp = value);
        },
        codeAutoRetrievalTimeout: (verificationId) {
          print(verificationId);
        },
      );
    });
    if (otp) {
      Navigator.pop(context);
    }
  }

  Padding buildAddress() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        controller: _addressCtrl,
        validator: (value) {
          if (value.isEmpty) {
            return 'Enter the field';
          }
          return null;
        },
        maxLines: 10,
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          labelText: 'Address',
          alignLabelWithHint: true,
        ),
      ),
    );
  }

  Padding buildColor(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
          showDialog(
            context: context,
            child: AlertDialog(
              title: const Text('Pick a color!'),
              content: SingleChildScrollView(
                child: ColorPicker(
                  pickerColor: _colorData,
                  onColorChanged: (value) {
                    setState(() {
                      _colorData = value;
                    });
                  },
                ),
              ),
              actions: <Widget>[
                FlatButton(
                  child: const Text('Got it'),
                  onPressed: () {
                    setState(() => currentColor = _colorData);
                    print(currentColor.toString());
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          );
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              flex: 8,
              child: TextFormField(
                enabled: false,
                validator: (value) => null,
                style: TextStyle(color: _colorData),
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: BorderSide(color: _colorData),
                  ),
                  labelText: 'Primary $_colorData',
                  labelStyle: TextStyle(color: _colorData),
                ),
              ),
            ),
            Expanded(
              flex: 2,
              child: Icon(
                Icons.brightness_1,
                color: _colorData,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Padding buildType() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: DropdownButtonFormField(
        validator: (value) => null,
        value: _typeData,
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
        ),
        items: [
          DropdownMenuItem(
            child: Text('Super Admin'),
            value: 'super_admin',
          ),
          DropdownMenuItem(
            child: Text('Admin'),
            value: 'admin',
          ),
        ],
        onChanged: (value) {
          setState(() {
            _typeData = value;
          });
        },
      ),
    );
  }

  Padding buildPhone() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        keyboardType: TextInputType.phone,
        controller: _phoneCtrl,
        validator: (value) {
          if (value.isEmpty) {
            return 'Enter the field';
          }
          return null;
        },
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          labelText: 'Phone Number',
        ),
      ),
    );
  }

  Padding buildName() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        controller: _nameCtrl,
        validator: (value) {
          if (value.isEmpty) {
            return 'Enter the field';
          }
          return null;
        },
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          labelText: 'Name',
        ),
      ),
    );
  }
}
