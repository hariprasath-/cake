import 'package:cake_app/Admin/Cakes/cake.dart';
import 'package:cake_app/Admin/Cakes/editCakes.dart';
import 'package:cake_app/Admin/admin_home.dart';
import 'package:cake_app/Admin/add_user.dart';
import 'package:cake_app/LoginScreen/login_screen.dart';
import 'package:cake_app/SplashScreen/splash_screen.dart';
import 'package:cake_app/SuperAdmin/AddUserScreen/add_user_screen.dart';
import 'package:cake_app/SuperAdmin/EditUserDetails/editBakeryDetails.dart';
import 'package:cake_app/SuperAdmin/HomeScreen/super_admin_home.dart';
import 'package:cake_app/User/screens/details/details_screen.dart';
import 'package:cake_app/User/screens/home/home_screen.dart';
import 'package:flutter/material.dart';



class RouteNames {
  static final splashScreen = SplashScreen.routeName;
  static final loginScreen = LoginScreen.routeName;
  static final superAdminHome = SuperAdminHome.routeName;
  static final addUser = AddUserScreen.routeName;
  static final adminHome = AdminHome.routeName;
 
  static final addCakes = AddCakes.routeName;
    static final details = Homepage.routeName;


  static final editBakery = EditBakeryDetails.routeName;
  static final editCakes = EditCakes.routeName;
  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    print(settings.name);
    if (settings.name == splashScreen)
      return MaterialPageRoute(
        builder: (context) => SplashScreen(),
      );
    if (settings.name == loginScreen)
      return MaterialPageRoute(
        builder: (context) => LoginScreen(),
      );

    if (settings.name == superAdminHome)
      return MaterialPageRoute(
        builder: (context) => SuperAdminHome(),
      );

    if (settings.name == addUser)
      return MaterialPageRoute(
        builder: (context) => AddUserScreen(),
      );
    // ignore: unrelated_type_equality_checks
    if (settings.name == adminHome)
      return MaterialPageRoute(
        builder: (context) => AdminHome(),
      );
   
    if (settings.name == addCakes)
      return MaterialPageRoute(
        builder: (context) => AddCakes(),
      );
    if (settings.name == editBakery)
      return MaterialPageRoute(
        builder: (context) => EditBakeryDetails(),
      );
       if (settings.name == details)
      return MaterialPageRoute(
        builder: (context) => DetailsScreen(),
      );
    if (settings.name == editCakes)
      return MaterialPageRoute(
        builder: (context) => EditCakes(),
      );
    return null;
  }
}
